//
//  PesquisaViewModel.swift
//  IoasysDesafio
//
//  Created by victor willyam on 10/26/21.
//

//import Foundation
//import Combine


//class PesquisaViewModel: ObservableObject {
//
//    var subscription : Set<AnyCancellable> = []
//
//    @Published private (set) var empresa: [Empresa] = []
//
//    @Published  var searchText: String = String()
//
//    //TODO: inicializador do empresa via model.
//
//    init() {
//        $searchText
//            .debounce(for: .milliseconds(800), scheduler: RunLoop.main) /* debounces o editor da string, de forma que atrasa o processo de envio de solicitação ao servidor remoto. */
//            .removeDuplicates()
//            .map({(string) -> String? in
//                if string.count < 1 {
//                    self.empresa = []
//                    return nil
//                }
//
//                return string
//            }) /* previne o envio de varios requests e envia nill caso a contagem de caracters seja menor que 1 */
//            .compactMap{ $0 } //remove os valores nil para que a string de pesquisa não seja passada para a cadeia do editor
//            .sink { (_) in
//                //
//            } receiveValue: { [self] (searchField) in
//                searchItems(searchText: searchField)
//
//            }.store(in: &subscription)
//    }
//
//    private func searchItems(searchText: String) {
//        NetworkManager.shared.sendRequest(to: ApiConstants.ProductSearchPath.description, model: Empresa.self, queryItem: ["nome": searchText])
//            .receive(on: RunLoop.main)
//            .sink { (completed ) in
//                //
//            } receiveValue: { [self] (searchedProducts) in
//                empresa = searchedProducts
//
//            }.store(in: &subscription)
//
//    }
//}
