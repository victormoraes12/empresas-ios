//
//  LoginUser.swift
//  IoasysDesafio
//
//  Created by victor willyam on 10/25/21.
//

import Foundation
import Alamofire

class EmpresaViewModel : ObservableObject {
    
    let url: String = "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in"
    
    func logar(login: String, senha: String, _ completion: @escaping (Bool)-> Void) {
        
        let user = User(email: login, password: senha)
        
        AF.request(url, method: .post, parameters: user, encoder: JSONParameterEncoder.default).response{ resposta in
            
            if let response = resposta.response,
               let accessToken = response.allHeaderFields["access-token"] as? String,
               let client = response.allHeaderFields["client"] as? String,
               let uid = response.allHeaderFields["uid"] as? String
            {
                
                print("Informações Heard")
                print(accessToken)
                print(client)
                print(uid)
                
                UserDefaults.standard.set("access-token", forKey: accessToken) //setObject
                UserDefaults.standard.set("client", forKey: client) //setObject
                UserDefaults.standard.set("uid", forKey: uid) //setObject
                
                completion(true)
                
            }
            else {
                
                completion(false)
                
            }
            
        }
        
    }
}

