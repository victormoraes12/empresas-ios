//
//  ConsultaEmpresa.swift
//  IoasysDesafio
//
//  Created by victor willyam on 10/25/21.


//import SwiftUI
//
//struct ConsultaEmpresa: View {
//    
//    let layout = [
//
//        GridItem(.flexible())
//    ]
//
//    @ObservedObject var pesquisaViewModel: PesquisaViewModel = PesquisaViewModel()
//    
//    var body: some View {
//        
//        VStack {
//            
//            HStack {
//                TextField("Procurar", text: $pesquisaViewModel.searchText)
//                    .padding(.horizontal, 40)
//                    .frame(width: UIScreen.main.bounds.width - 110, height: 45, alignment: .leading)
//                    .background(Color(#colorLiteral(red: 0.9294475317, green: 0.9239223003, blue: 0.9336946607, alpha: 1)))
//                    .clipped()
//                    .cornerRadius(10)
//                    .overlay(
//                        HStack{
//                            Image(systemName: "magnifyingglass")
//                                .foregroundColor(.gray)
//                                .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
//                                .padding(.leading, 16)
//                        }
//                    )
//                
//                Spacer()
//                
//            }.padding().padding(.top,30)
//            
//            
//            if pesquisaViewModel.empresa.count > 0 {
//                ScrollView(showsIndicators: false) {
//                    LazyVGrid(columns: layout, spacing: 12) {
//                        ForEach(pesquisaViewModel.empresa, id: \.self) { item in
//                            EmpresaView(item: item)
//                        }
//                    }
//                }
//            } else {
//                
//                Spacer()
//                
//                Text("Qual empresa está procurando?")
//                
//                Spacer()
//                
//            }
//            
//        }
//        .background(Color(#colorLiteral(red: 0.9758560663, green: 0.9758560663, blue: 0.9758560663, alpha: 1)))
//        .edgesIgnoringSafeArea(.all)
//        
//    }
//    
//}
//
//
//
//struct EmpresaView: View {
//    
//    let item : Empresa
//    
//    var body: some View {
//        
//        ZStack {
//            HStack(spacing: 5) {
//                RoundedRectangle(cornerRadius: 10, style: .circular)
//                    .fill(Color.gray.opacity(0.4))
//                    .frame(height: 80)
//                    .frame(width: 70)
//                    .padding(.horizontal)
//                
//                
//                VStack(alignment: .leading, spacing: 5){
//                    Text(item.name!)
//                        .font(.body)
//                        .fontWeight(.semibold)
//                    Text(item.flavour!)
//                        .fontWeight(.regular)
//                        .font(.caption)
//                    Text(item.weight!)
//                        .foregroundColor(.gray)
//                        .fontWeight(.regular)
//                        .font(.caption)
//                }
//                
//                Spacer()
//                
//                
//            }
//        }
//        .frame(height: 100)
//        .background(
//            RoundedRectangle(cornerRadius: 15)
//                .fill(Color.white)
//                .shadow(color: Color.black.opacity(0.11), radius: 8, x: /*@START_MENU_TOKEN*/0.0/*@END_MENU_TOKEN*/, y: 7)
//        )
//        
//        .padding(.horizontal, 10)
//        .animation(.easeIn)
//        
//    }
//}
//
//struct ConsultaEmpresa_Previews: PreviewProvider {
//    static var previews: some View {
//        ConsultaEmpresa()
//    }
//}
