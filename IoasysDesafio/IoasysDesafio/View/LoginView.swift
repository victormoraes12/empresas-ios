//
//  ContentView.swift
//  IoasysDesafio
//
//  Created by victor willyam on 10/23/21.
//

import SwiftUI

struct LoginView: View {
    
    @State var email = ""
    @State var senha = ""
    @State var login = false
    @State var signup = false
    
    var body: some View {
        
        ZStack{
            
            LinearGradient(gradient: .init(colors: [Color("1"),Color("2")]), startPoint: .leading, endPoint: .trailing).edgesIgnoringSafeArea(.all)
            
            
            Login(login: $login, signup: $signup, email: $email, senha: $senha)
            
            
        }
        
        .sheet(isPresented: $signup) {
            
            signUp(signup: self.$signup)
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

struct Login : View {
       
    @Binding var login : Bool
    @Binding var signup : Bool
    @Binding var email : String
    @Binding var senha : String
    @State var successLogin: Bool = false
    
    let viewModel = EmpresaViewModel()
    
    var body : some View{
        
        VStack(alignment: .center, spacing: 22, content: {
            
            Image("logo").resizable().frame(width: 80, height: 80).padding(.bottom, 15)
            
            HStack{
                
                Image(systemName: "person.fill").resizable().frame(width: 20, height: 20)
                
                TextField("E-mail", text: $email).padding(.leading, 12).font(.system(size: 20))
                
            }.padding(12)
                .background(Color("Color1"))
                .cornerRadius(20)
            
            HStack{
                
                Image(systemName: "lock.fill").resizable().frame(width: 15, height: 20).padding(.leading, 3)
                
                SecureField("Senha", text: $senha).padding(.leading, 12).font(.system(size: 20))
                
            }.padding(12)
                .background(Color("Color"))
                .cornerRadius(20)
            
            Button(action: {
                
                //TODO: transição de tela para ConsultaEmpresa
                
                self.login.toggle()
                viewModel.logar(login: email, senha: senha) { sucesso in
                    
                    successLogin = sucesso
                    
                    if sucesso {
                        
                        print("logado")
                       
                    }
                    
                }
                
            }) {
                
                Text("Acessar").foregroundColor(.white).padding().frame(width: 150)
                
            }
            .background(LinearGradient(gradient: .init(colors: [Color("1"),Color("2")]), startPoint: .leading, endPoint: .trailing))
            .cornerRadius(20)
            .shadow(radius: 25)
            .fullScreenCover(isPresented: $successLogin,content: {
              
                InicialView()
            
            })
            
            Button(action: {
                
            }) {
                
                Text("Esqueceu a senha?").foregroundColor(.white)
            }
            
            VStack{
                
                Text("Ainda não possui uma conta?").foregroundColor(.white)
                
                Button(action: {
                    
                    
                    self.signup.toggle()
                    
                }) {
                    
                    Text("Inscrever-se").foregroundColor(.white).padding().frame(width: 150)
                    
                }
                .background(LinearGradient(gradient: .init(colors: [Color("1"),Color("2")]), startPoint: .leading, endPoint: .trailing))
                .cornerRadius(20)
                .shadow(radius: 25)
                
            }.padding(.top, 35)
            
        })
            .padding(.horizontal, 18)
    }
}



struct signUp : View  {
    
    @Binding var signup : Bool
    @State var user : String = ""
    @State var pass : String = ""
    @State var repass : String = ""
    
    var body : some View{
        
        
        ZStack{
            
            LinearGradient(gradient: .init(colors: [Color("1"),Color("2")]), startPoint: .leading, endPoint: .trailing).edgesIgnoringSafeArea(.all)
            
            
            VStack(alignment: .center, spacing: 22, content: {
                
                Image("logo").resizable().frame(width: 100, height: 100).padding(.bottom, 15)
                
                HStack{
                    
                    Image(systemName: "person.fill").resizable().frame(width: 20, height: 20)
                    
                    TextField("E-mail", text: $user).padding(.leading, 12).font(.system(size: 20))
                    
                }.padding(12)
                    .background(Color("Color"))
                    .cornerRadius(20)
                
                HStack{
                    
                    Image(systemName: "lock.fill").resizable().frame(width: 15, height: 20).padding(.leading, 3)
                    
                    SecureField("Senha", text: $repass).padding(.leading, 12).font(.system(size: 20))
                    
                }.padding(12)
                    .background(Color("Color"))
                    .cornerRadius(20)
                
                HStack{
                    
                    Image(systemName: "lock.fill").resizable().frame(width: 15, height: 20).padding(.leading, 3)
                    
                    SecureField("Repetir senha", text: $pass).padding(.leading, 12).font(.system(size: 20))
                    
                }.padding(12)
                    .background(Color("Color"))
                    .cornerRadius(20)
                
                Button(action: {
                    
                    self.signup.toggle()
                    
                }) {
                    
                    Text("Inscrever-se").foregroundColor(.white).padding().frame(width: 150)
                    
                }
                .background(LinearGradient(gradient: .init(colors: [Color("1"),Color("2")]), startPoint: .leading, endPoint: .trailing))
                .cornerRadius(20)
                .shadow(radius: 25)
                
                
            })
                .padding(.horizontal, 18)
            
        }
        
    }
}
