//
//  InicialView.swift
//  IoasysDesafio
//
//  Created by victor willyam on 10/26/21.
//

import SwiftUI

struct InicialView: View {
    var logar: EmpresaViewModel = EmpresaViewModel()
    
    var body: some View {
        Text("Você logou")
    }
}

struct InicialView_Previews: PreviewProvider {
    static var previews: some View {
        InicialView()
    }
}
