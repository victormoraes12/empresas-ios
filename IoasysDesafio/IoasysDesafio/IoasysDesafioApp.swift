//
//  IoasysDesafioApp.swift
//  IoasysDesafio
//
//  Created by victor willyam on 10/23/21.
//

import SwiftUI

@main
struct IoasysDesafioApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}
